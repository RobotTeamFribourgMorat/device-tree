# Purpos : PWM Initialisation Script for BBB kernel v4.1
#          Please run this script at stratup
# Author : Jonathan Peclat
# Date : 14.02.2017

export ehrpwm0=/sys/devices/platform/ocp/48300000.epwmss/48300200.ehrpwm/pwm/pwmchip*
export ehrpwm1=/sys/devices/platform/ocp/48302000.epwmss/48302200.ehrpwm/pwm/pwmchip*
export ehrpwm2=/sys/devices/platform/ocp/48304000.epwmss/48304200.ehrpwm/pwm/pwmchip*

echo 0 > ${ehrpwm0}/export
echo 1 > ${ehrpwm0}/export
echo 0 > ${ehrpwm1}/export
echo 1 > ${ehrpwm1}/export
echo 0 > ${ehrpwm2}/export
echo 1 > ${ehrpwm2}/export

echo 20000 > ${ehrpwm0}/pwm0/period
echo 20000 > ${ehrpwm0}/pwm1/period
echo 20000 > ${ehrpwm1}/pwm0/period
echo 20000 > ${ehrpwm1}/pwm1/period
echo 20000 > ${ehrpwm2}/pwm0/period
echo 20000 > ${ehrpwm2}/pwm1/period

echo 0 > ${ehrpwm0}/pwm0/duty_cycle
echo 0 > ${ehrpwm0}/pwm1/duty_cycle
echo 0 > ${ehrpwm1}/pwm0/duty_cycle
echo 0 > ${ehrpwm1}/pwm1/duty_cycle
echo 0 > ${ehrpwm2}/pwm0/duty_cycle
echo 0 > ${ehrpwm2}/pwm1/duty_cycle

echo 1 > ${ehrpwm0}/pwm0/enable
echo 1 > ${ehrpwm0}/pwm1/enable
echo 1 > ${ehrpwm1}/pwm0/enable
echo 1 > ${ehrpwm1}/pwm1/enable
echo 1 > ${ehrpwm2}/pwm0/enable
echo 1 > ${ehrpwm2}/pwm1/enable

