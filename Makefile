include func.mk # For rwildcard


DEVICE_TREE_DIR := $(shell pwd)
DTC := dtc

SRC_DIR := $(DEVICE_TREE_DIR)/src
PRE_DIR := $(DEVICE_TREE_DIR)/pre
INC_DIR := $(DEVICE_TREE_DIR)/include
OBJ_DIR := $(DEVICE_TREE_DIR)/out
CPP_DT_FLAGS := -x assembler-with-cpp

DTBO_SRC_FILES := $(call rwildcard, $(SRC_DIR)/, *.dtbo.dts)
DTBO_PRE_FILES := $(DTBO_SRC_FILES:$(SRC_DIR)/%.dtbo.dts=$(PRE_DIR)/%.dtbo.dts)
DTBO_OBJ_FILES := $(DTBO_SRC_FILES:$(SRC_DIR)/%.dtbo.dts=$(OBJ_DIR)/%.dtbo)
DTB_SRC_FILES := $(call rwildcard, $(SRC_DIR)/, *.dtb.dts)
DTB_PRE_FILES := $(DTB_SRC_FILES:$(SRC_DIR)/%.dtb.dts=$(PRE_DIR)/%.dtb.dts)
DTB_OBJ_FILES := $(DTB_SRC_FILES:$(SRC_DIR)/%.dtb.dts=$(OBJ_DIR)/%.dtb)

all: mkdir $(DTBO_OBJ_FILES) # $(DTB_OBJ_FILES)
mkdir:
	mkdir -p $(INC_DIR) $(PRE_DIR) $(OBJ_DIR)

$(DTBO_OBJ_FILES): $(OBJ_DIR)/%.dtbo : $(PRE_DIR)/%.dtbo.dts
	$(DTC) -O dtb -o $@ -b 0 -@ $<

$(DTB_OBJ_FILES): $(OBJ_DIR)/%.dtb : $(PRE_DIR)/%.dtb.dts
	$(DTC) -O dtb -i$(INC_DIR) -o $@ -b 0 -@ $<

$(DTBO_PRE_FILES): $(PRE_DIR)/%.dts : $(SRC_DIR)/%.dts
	cpp $(CPP_DT_FLAGS) $(INC_DIR:%=-I%) $< | grep "^[^#;]" > $@

$(DTB_PRE_FILES): $(PRE_DIR)/%.dts : $(SRC_DIR)/%.dts
	cpp $(CPP_DT_FLAGS) $(INC_DIR:%=-I%) $< | grep "^[^#;]" > $@

clean:
	rm -rf $(PRE_DIR)
	rm -rf $(OBJ_DIR)
