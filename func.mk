# Recursive wildcard function
# Usage: $(call rwildcard, foo/, *.cpp)
rwildcard = $(foreach d, $(wildcard $1*), $(call rwildcard, $d/, $2) $(filter $(subst *, %, $2), $d))

